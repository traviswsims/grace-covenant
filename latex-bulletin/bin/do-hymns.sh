#!/usr/bin/env zsh

TPH="/Users/travis/Documents/Trinity Psalter Hymnal 2020"
TO="./images/hymns"

# ./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/23 Hymns-God-Holiness and Justice (229-230)/TPH 230.png" "${TO}/tph-230.png"
# ./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/32 Hymns-Jesus Christ-Priest (274-278)/TPH 278.png"      "${TO}/tph-278.png"
# ./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/55 Hymns-Salvation-Sanctification (463-467)/TPH 465.png" "${TO}/tph-465.png"


# hymns=("213" "219" "222" "224" "243" "247" "249" "250" "276" "281" "282" "283" "291" "293" "294" "299" "300" "308" "313" "315" "317" "318" "319" "323" "336" "338" "352" "363" "374" "380" "403" "425" "433" "446" "452" "456" "459" "538" "393" "293" "93" "493" "98C" "99B")

# for hymn in $hymns; do
  # echo "\\HymnPage{tph-${hymn}}" > "partials/hymns/tph-${hymn}.tex"
# done


# hymns=("103A" "213" "216" "219" "222" "224" "227" "236" "243" "244" "245" "247" "248" "249" "250" "276" "277" "281" "282" "283" "291" "293" "294" "299" "300" "308" "311" "313" "315" "317" "318" "319" "323" "336" "338" "340" "352" "360" "363" "367" "374" "380" "403" "425" "431" "433" "446" "452" "456" "459" "526" "538" "93" "98C" "99B")

# for hymn in $hymns; do
  # fpng $hymn;
# done
#
#


# Single page hymns

# ./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/18 Hymns-God-The Trinity (208-213)/TPH 213.png" ./images/hymns/tph-213.png
# ./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/19 Hymns-God-His Perfections (214-220)/TPH 219.png" ./images/hymns/tph-219.png
# ./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/20 Hymns-God-Infinity and Eternity (221-223)/TPH 222.png" ./images/hymns/tph-222.png
# ./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/21 Hymns-God-Wisdom (224-226)/TPH 224.png" ./images/hymns/tph-224.png
# ./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/26 Hymns-God-Faithfulness (243-247)/TPH 243.png" ./images/hymns/tph-243.png
# ./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/26 Hymns-God-Faithfulness (243-247)/TPH 247.png" ./images/hymns/tph-247.png
# ./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/27 Hymns-God-Creation (248-254)/TPH 249.png" ./images/hymns/tph-249.png
# ./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/27 Hymns-God-Creation (248-254)/TPH 250.png" ./images/hymns/tph-250.png
# ./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/32 Hymns-Jesus Christ-Priest (274-278)/TPH 276.png" ./images/hymns/tph-276.png
# ./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/33 Hymns-Jesus Christ-King (279-281)/TPH 281.png" ./images/hymns/tph-281.png
# ./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/34 Hymns-Jesus Christ-Praise of Christ (282-291)/TPH 282.png" ./images/hymns/tph-282.png
# ./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/34 Hymns-Jesus Christ-Praise of Christ (282-291)/TPH 283.png" ./images/hymns/tph-283.png
# ./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/34 Hymns-Jesus Christ-Praise of Christ (282-291)/TPH 291.png" ./images/hymns/tph-291.png
# ./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/35 Hymns-Jesus Christ-Advent (292-302)/TPH 293.png" ./images/hymns/tph-293.png
# ./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/35 Hymns-Jesus Christ-Advent (292-302)/TPH 294.png" ./images/hymns/tph-294.png
# ./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/35 Hymns-Jesus Christ-Advent (292-302)/TPH 299.png" ./images/hymns/tph-299.png
# ./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/35 Hymns-Jesus Christ-Advent (292-302)/TPH 300.png" ./images/hymns/tph-300.png
# ./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/36 Hymns-Jesus Christ-Birth (303-324)/TPH 308.png" ./images/hymns/tph-308.png
# ./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/36 Hymns-Jesus Christ-Birth (303-324)/TPH 313.png" ./images/hymns/tph-313.png
# ./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/36 Hymns-Jesus Christ-Birth (303-324)/TPH 315.png" ./images/hymns/tph-315.png
# ./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/36 Hymns-Jesus Christ-Birth (303-324)/TPH 317.png" ./images/hymns/tph-317.png
# ./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/36 Hymns-Jesus Christ-Birth (303-324)/TPH 318.png" ./images/hymns/tph-318.png
# ./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/36 Hymns-Jesus Christ-Birth (303-324)/TPH 319.png" ./images/hymns/tph-319.png
# ./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/36 Hymns-Jesus Christ-Birth (303-324)/TPH 323.png" ./images/hymns/tph-323.png
# ./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/38 Hymns-Jesus Christ-Passion and Atoning Death (335-354)/TPH 336.png" ./images/hymns/tph-336.png
# ./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/38 Hymns-Jesus Christ-Passion and Atoning Death (335-354)/TPH 338.png" ./images/hymns/tph-338.png
# ./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/38 Hymns-Jesus Christ-Passion and Atoning Death (335-354)/TPH 352.png" ./images/hymns/tph-352.png
# ./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/39 Hymns-Jesus Christ-Resurrection (355-369)/TPH 363.png" ./images/hymns/tph-363.png
# ./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/41 Hymns-Jesus Christ-Exaltation (374-382)/TPH 374.png" ./images/hymns/tph-374.png
# ./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/41 Hymns-Jesus Christ-Exaltation (374-382)/TPH 380.png" ./images/hymns/tph-380.png
# ./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/44 Hymns-The Church-Church of Christ (402-405)/TPH 403.png" ./images/hymns/tph-403.png
# ./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/48 Hymns-Salvation-Election (425-428)/TPH 425.png" ./images/hymns/tph-425.png
# ./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/49 Hymns-Salvation-Salvation by Grace (429-437)/TPH 433.png" ./images/hymns/tph-433.png
# ./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/51 Hymns-Salvation-Union with Christ (446-448)/TPH 446.png" ./images/hymns/tph-446.png
# ./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/52 Hymns-Salvation-Faith (449-456)/TPH 452.png" ./images/hymns/tph-452.png
# ./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/52 Hymns-Salvation-Faith (449-456)/TPH 456.png" ./images/hymns/tph-456.png
# ./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/53 Hymns-Salvation-Justification (457-460)/TPH 459.png" ./images/hymns/tph-459.png
# ./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/68 Hymns-Christian Life-Christian Service (534-538)/TPH 538.png" ./images/hymns/tph-538.png
# ./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/43 Hymns-The Holy Spirit (391-401)/TPH 393.png" ./images/hymns/tph-393.png
# ./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/35 Hymns-Jesus Christ-Advent (292-302)/TPH 293.png" ./images/hymns/tph-293.png
# ./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/04 Psalm 90-106/TPH 93.png" ./images/hymns/tph-93.png
# ./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/60 Hymns-Christian Life-Love for Christ (491-499)/TPH 493.png" ./images/hymns/tph-493.png
# ./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/04 Psalm 90-106/TPH 98C.png" ./images/hymns/tph-98C.png
# ./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/04 Psalm 90-106/TPH 99B.png" ./images/hymns/tph-99B.png








# Two page hymns

./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/04 Psalm 90-106/TPH 103A-2.png" ./images/hymns/tph-103A-2.png
./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/04 Psalm 90-106/TPH 103A-1.png" ./images/hymns/tph-103A-1.png
./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/19 Hymns-God-His Perfections (214-220)/TPH 216-2.png" ./images/hymns/tph-216-2.png
./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/19 Hymns-God-His Perfections (214-220)/TPH 216-1.png" ./images/hymns/tph-216-1.png
./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/22 Hymns-God-Power (227-228)/TPH 227-1.png" ./images/hymns/tph-227-1.png
./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/22 Hymns-God-Power (227-228)/TPH 227-2.png" ./images/hymns/tph-227-2.png
./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/25 Hymns-God-Love and Grace (234-242)/TPH 236-1.png" ./images/hymns/tph-236-1.png
./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/25 Hymns-God-Love and Grace (234-242)/TPH 236-2.png" ./images/hymns/tph-236-2.png
./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/26 Hymns-God-Faithfulness (243-247)/TPH 244-2.png" ./images/hymns/tph-244-2.png
./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/26 Hymns-God-Faithfulness (243-247)/TPH 244-1.png" ./images/hymns/tph-244-1.png
./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/26 Hymns-God-Faithfulness (243-247)/TPH 245-1.png" ./images/hymns/tph-245-1.png
./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/26 Hymns-God-Faithfulness (243-247)/TPH 245-2.png" ./images/hymns/tph-245-2.png
./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/27 Hymns-God-Creation (248-254)/TPH 248-1.png" ./images/hymns/tph-248-1.png
./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/27 Hymns-God-Creation (248-254)/TPH 248-2.png" ./images/hymns/tph-248-2.png
./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/32 Hymns-Jesus Christ-Priest (274-278)/TPH 277-2.png" ./images/hymns/tph-277-2.png
./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/32 Hymns-Jesus Christ-Priest (274-278)/TPH 277-1.png" ./images/hymns/tph-277-1.png
./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/36 Hymns-Jesus Christ-Birth (303-324)/TPH 311-2.png" ./images/hymns/tph-311-2.png
./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/36 Hymns-Jesus Christ-Birth (303-324)/TPH 311-1.png" ./images/hymns/tph-311-1.png
./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/38 Hymns-Jesus Christ-Passion and Atoning Death (335-354)/TPH 340-2.png" ./images/hymns/tph-340-2.png
./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/38 Hymns-Jesus Christ-Passion and Atoning Death (335-354)/TPH 340-1.png" ./images/hymns/tph-340-1.png
./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/39 Hymns-Jesus Christ-Resurrection (355-369)/TPH 360-1.png" ./images/hymns/tph-360-1.png
./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/39 Hymns-Jesus Christ-Resurrection (355-369)/TPH 360-2.png" ./images/hymns/tph-360-2.png
./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/39 Hymns-Jesus Christ-Resurrection (355-369)/TPH 367-2.png" ./images/hymns/tph-367-2.png
./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/39 Hymns-Jesus Christ-Resurrection (355-369)/TPH 367-1.png" ./images/hymns/tph-367-1.png
./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/49 Hymns-Salvation-Salvation by Grace (429-437)/TPH 431-2.png" ./images/hymns/tph-431-2.png
./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/49 Hymns-Salvation-Salvation by Grace (429-437)/TPH 431-1.png" ./images/hymns/tph-431-1.png
./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/66 Hymns-Christian Life-Pilgrimage and Guidance (523-530)/TPH 526-1.png" ./images/hymns/tph-526-1.png
./bin/fix-image "${TPH}/TPH Unlocked PDF 2019 TPH2020/66 Hymns-Christian Life-Pilgrimage and Guidance (523-530)/TPH 526-2.png" ./images/hymns/tph-526-2.png
