#!/usr/bin/env bash

set -e

export CF_Key="71df0ad9a6f2cf1ebf5a85f54f44b339ac598"
export CF_Email="vivid.boat9798@fastmail.com"

DOMAIN="gccreformed.com"
WWW_DOMAIN="www.${DOMAIN}"

pushd "$HOME/bin/acme.sh"

./acme.sh --issue --force --dns dns_cf -d $DOMAIN -d $WWW_DOMAIN
./acme.sh --deploy --deploy-hook cpanel_uapi --domain $DOMAIN --domain $WWW_DOMAIN

popd
