To run the server:

bundle exec middleman

Then view in your browser:  http://localhost:4567/

Then open VS Code, Vim, etc.

All files you need to edit are in ./source

When finished, commit your changes:

git add .
git commit -m "stuff I did here"
git push