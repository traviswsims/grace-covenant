# General Notes

Thank you for serving our Lord and Grace Covenant Church!  Many in the congregation will
appreciate the help you're providing to enhance our worship.


1. All equipment needs to be charged each week.  The camera takes a long time to charge.
2. Pastor Dick's mic needs 2 double AA batteries.  Use fresh batteries each week of the same brand and age.

# Equipment Identification

**U3 receiver** – the radio receiver we plug into the speakers.  Has a flat bottom.  They're all the same.

**U3 transmitter** – the radio transmitter we use to broadcast to the speakers.  Has a bump at the bottom.  There are
two types:  "Mic" and "48V".  Use 48 volt for the mic near the music stand, "Mic" for everything else.

**Mixer** – the big sound board with all the buttons

**Lavalier Microphone** the lapel mic that Pastor Dick wears.  The transmitter has 1 antenna, the receiver has 2
antennas.

**Zoom H5** – about the size of your hand.  Has 2 silver crossed microphones at the top and 2 XLR inputs at the bottom.

# Speakers

### Front speakers

1.  Connect a U3 receiver on radio **channel 1** to the speaker.
2.  Ensure that the receiver is on.
3.  Plug in the speaker and turn it on.
4.  Make sure volume knob is turned up to the 12-o'clock position.

### Break-room speaker

1.  Connect a U3 receiver on radio **channel 1** to the speaker.
2.  Ensure that the receiver is on.
3.  Plug in the speaker and turn it on.  After a second or two, the light on the front should be orange.
4.  On the front panel, set the gain knob to "Mic" and the volume to the 12 o'clock position.

# Mixer

1.  Plug in the mixer and turn it on.  **Before connecting anything, make sure all levels are turned down and muted.**
2.  Use an XLR cable to connect a U3 Mic Transmitter to the mixer output.  The U3 Mic transmitter should be on radio **channel 1**.
    Plug the other end to the **headphone** jack on the receiver for Pastor Dick's mic (see below).

# Audio interface

1.  Grab the Zoom H5 audio interface, and connect it to one of the 2 mixer output channels (top right of the mixer) via an XLR cable.
2.  Connect the audio interface to a computer via a USB cable.
3.  Hold down the power button on the Zoom H5 to turn it on.
4.  Select "Audio Interface" and "PC/Mac bus powered"
5.  Set the audio level knobs to 5 for both channels on the Zoom H5.
6.  If the lights on the H5 are not lit up (red) for channels 1 and 2, they are muted.  Push the `1` and `2` buttons to
    unmute them.

# Condenser Microphones (front near the music stand)

1.  Connect a condenser microphone via XLR cable to a U3 transmitter with *48V* output (not the "Mic" transmitter).
2.  Turn the 48V U3 transmitter on and select radio **channel 4**.
3.  Install the microphone and transmitter onto the microphone stand and place it up front.
4.  Grab a U3 receiver and select radio **channel 4**.
5.  Plug the receiver into the mixer via an XLR cable on any channel (4 is a good choice for consistency, but it doesn't
    matter).


# Lavalier Microphone (Pastor Dick's mic)

### Receiver

1.  Take the lavalier microphone ("lav mic") receiver (2 antennas) and plug it in via USB power.
2.  Make sure the red 3.5mm (headphone) cable is plugged into the *headphone* jack and connected to the adapter on mixer
    **channel 1**.
3.  Hold down the power button until it turns on.

### Transmitter

1.  Take the lav mic transmitter and connect the microphone by screwing it into the mic input.  Do not over-tighten it.
2.  Replace the batteries with 2 new AA batteries of the same brand and age.
3.  When Pastor Dick is ready, turn the transmitter on.  Make sure the transmitter and receiver are on the same channel
    (i.e., both on `A`, or both on `B`).
4.  **Important:**  Make sure the transmitter (one antenna) has **two solid green lights**.  If the left light is red, press the mute button
    on transmitter to unmute it.  If the right light is blinking, make sure the receiver is on and both are set to the
    same channel.
5.  Speak into microphone and watch the meters on the *receiver* (2 antennas, connected to the mixer) and the Zoom H5.
    If both meters move, you have correctly configured the system.  Otherwise, troubleshoot your connections and repeat
    the steps above.

# Camera

The camera's built in audio is terrible, so we need to use the audio from the mixer.

1.  Connect a mini-HDMI cable to the camera.  Connect other (HDMI) end to the video capture card in the *input* slot.
2.  Configure the laptop to use the audio from the Zoom H5 and the video from the camera.  Record on the laptop and
    save.

# During worship

1.  Mute all microphones via the mixer at all times unless in-use for worship.
2.  Unmute Pastor Dick when he comes up front to speak, and mute him during the hymns.
3.  Keep the condenser mic (**channel 4**) muted at all times.  Only unmute it during the morning prayer if Pastor Dick is
    not singing.  It usually needs to be turned up to the 11 or 12 o'clock position if Jim is praying.
