function embedLatestSermon() {
  fetch('latest_sermon.html')
    .then((response) => {
      if (response.status === 200) {
        response.text()
          .then((iframe) => {
            document.getElementById('latest_sermon_embed').innerHTML = iframe;
            document.getElementById('latest_sermon_wrapper').classList.remove("d-none");
          })
      }
    })
}
