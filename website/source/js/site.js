Modernizr.on('webp', function(result) {
  if (result) {
    $('.modernizr-webp').addClass('webp')
  } else {
    $('.modernizr-webp').addClass('no-webp')
  }
});
